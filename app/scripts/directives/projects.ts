/// <reference path="../app.ts" />

'use strict';

module Searcher {

  export interface IProjectScope extends ng.IScope {
    project?: App.Core.Devv.IProject;
  }

  /**
   * @ngdoc directive
   * @name searcherApp.directive:projectCompact
   * @scope
   * @restrict EA
   *
   * @description
   * # projectCompact
   * Renderes Compact version of IProject data
   * @param {IProject}  project   Project data
   * @example
   *
   * `<project-compact project="data"></project-compact>`
   */
  export class Project implements ng.IDirective {
    templateUrl = 'templates/project-compact.html';
    restrict = 'EA';
    scope = {
      project: '='
    };
    replace = true;

    $scope: IProjectScope;
    link = (scope: IProjectScope, element:ng.IAugmentedJQuery, attrs:ng.IAttributes):void => {
      this.$scope = scope;
    }
  }

  export function projectFactory() {
    return new Searcher.Project();
  }
}

angular.module('searcherApp')
  .directive('projectCompact', Searcher.projectFactory);
