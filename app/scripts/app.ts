/// <reference path="inc.d.ts" />

'use strict';

/**
 * @ngdoc overview
 * @name searcherApp
 * @description
 * # searcherApp
 *
 * Main module of the application.
 */
angular.module('searcherApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  //'ui.bootstrap'
])
  .config(($routeProvider:ng.route.IRouteProvider) => {
    $routeProvider
      // NOTE: disabled main/about views

      /*.when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainViewCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutViewCtrl',
        controllerAs: 'about'
      })*/
      .when('/search', {
        templateUrl: 'views/search.html',
        controller: 'SearchViewCtrl',
        controllerAs: 'search'
      })
      .otherwise({
        redirectTo: '/search'
      });
  });
