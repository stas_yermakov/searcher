/// <reference path="../inc.d.ts" />

'use strict';

module Searcher {
  export interface IAboutViewScope extends ng.IScope {
  }

  /**
   * @ngdoc controller
   * @name searcherApp.controller:AboutViewCtrl
   * @description
   * # AboutViewCtrl
   * Controller of the searcherApp
   */
  export class AboutViewCtrl {

    constructor(private $scope:IAboutViewScope) {
    }
  }
}

angular.module('searcherApp')
  .controller('AboutViewCtrl', Searcher.AboutViewCtrl);
