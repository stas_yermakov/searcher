/// <reference path="../inc.d.ts" />

'use strict';

module Searcher {
  export interface IMainViewScope extends ng.IScope {
  }

  /**
   * @ngdoc controller
   * @name searcherApp.controller:MainViewCtrl
   * @description
   * # MainViewCtrl
   * Controller of the searcherApp
   */
  export class MainViewCtrl {
    constructor(private $scope:IMainViewScope) {
    }
  }
}

angular.module('searcherApp')
  .controller('MainViewCtrl', Searcher.MainViewCtrl);
