/// <reference path="../inc.d.ts" />

'use strict';

module Searcher {

  export interface ISearchViewScope extends ng.IScope {
    query?: string; // query value used to search projects

    projects: App.Core.Resource<App.Core.Devv.IProject[]>; // Resource with list of projects
  }

  /**
   * @ngdoc controller
   * @name searcherApp.controller:SearchViewCtrl
   * @class Searcher.SearchViewCtrl
   * @description
   * # SearchViewCtrl
   * Search control, holds query input and loads Projects list from API
   */
  export class SearchViewCtrl {
    static $inject = [
      '$scope',
      'projectsSearch'
    ];

    // Binded version of $scope.$digest and deferred to avoid $digest call inside other $digest loop
    private digestBind = _.defer.bind(_, () => this.$scope.$digest());

    constructor(
      private $scope:ISearchViewScope,
      private projectsSearch: Searcher.ProjectsSearch
    ) {
      this.$scope.projects = new App.Core.Resource<App.Core.Devv.IProject[]>();

      this.$scope.$watch('query', _.debounce(this.loadProjects.bind(this), 250));
    }

    /**
     * @ngdoc method
     * @name loadProjects
     * @methodOf Searcher.SearchViewCtrl
     * @description
     * Loads list of Projects according to query input
     */
    loadProjects() {
      if (this.$scope.query && this.$scope.query.length) {
        this.$scope.projects.loadData(this.projectsSearch.getProjects(this.$scope.query));

        // we have to call $digest here, because of debounce (timeout) we are out of $digest loop
        this.digestBind(); // digest scope, because this.$scope.projects.loading now is true
      }
    }
  }
}

angular.module('searcherApp')
  .controller('SearchViewCtrl', Searcher.SearchViewCtrl);
