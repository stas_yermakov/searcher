'use strict';

module App.Core.Devv {

  /**
   * @interface App.Core.Devv.IMatchConfig
   * @description
   * # IMatchConfig
   * Object that holds processed Query string, to be used while searching matches
   */
  export interface IMatchConfig {
    equal: string[];        // array of strings should be found in match candidate
    negative?: string[];    // array of strings should not be found in match candidate
    same: number[];         // array of indexes from equal array in case of duplicates is search query
                            // e.g. if equal:['a', 'b', 'b'] -> same:[-1,-1,1]
  }

  /**
   * @interface App.Core.Devv.IMatchFootprint
   * @description
   * # IMatchFootprint
   * Object that holds stats about matched string
   */
  export interface IMatchFootprint {
    exactMatch: boolean;    // true if each word from query found in string
    count: number;          // number of words from query found in string
    wordsCloseness: number; // counts distance between matched words
    entries?: number[][];   // array of indexes, each word from query was found, or null if not, .e.g `[[0, 4], [5,9], null]`
    match?: number[];       // array of number, each number is count word from query was found times, .e.g `[1, 1, 0]`
    matchCount: number;     // count of matched entries in examined string, .e.g. `2`
  }

  /**
   * Maps array of string to array of numbers, each of number is position in array of last matched item
   * or -1 if not found
   * e.g. if ['a', 'b', 'b'] -> [-1,-1,1]
   * e.g. if ['a', 'c', 'b', 'b', 'c'] -> [-1,-1,-1,2,1]
   * @param {string[]} items
   * @returns {number[]}
   */
  var findSameEntries = function (items:string[]):number[] {
    return _.map(items, (str:string, index:number) => {
      return index ? _.lastIndexOf(items, str, index - 1) : -1;
    });
  },
    /**
     * Creates array of indexes, each index is position of `str` entry in examined `value` string
     * @param {string} value examined string
     * @param {string} str string to search for
     * @returns {number[]}
     */
    findEntries = function(value: string, str: string) {
      var entries: number[] = [],
        lastIndex = 0,
        index: number;

      while((index = value.indexOf(str, lastIndex)) > -1) {
        entries.push(index);
        lastIndex = index + str.length;
      }

      return entries;
    };

  /**
   * @class App.Core.Devv.SearchFilter
   * @description
   * # SearchFilter
   * Holds static methods to match / highlight string values by Query string
   */
  export class SearchFilter {

    /**
     * @ngdoc method
     * @name findEntries
     * @methodOf App.Core.Devv.Project
     * @description
     * Generates array of indexes, Query words found in examined string
     * e.g. "test query here" search in "Other test query" -> [ [6,10], [11,16], null]
     * @param {string} value
     * @param {IMatchConfig} config
     * @returns {number[][]} array of indexes of found words or nulls if not found
     */
    static findEntries(value:string, config:IMatchConfig):number[][] {
      if (!value || !config)
        return null;

      var lowcased = value.toLowerCase();

      // Iterate 'equal' strings find it in value with indexOf
      return _.reduce(config.equal, (memo:number[][], equal:string, ind:number) => {
        // starting from 0 or end index of found duplicate
        // (in case of duplicates in search query, e.g. 'test line test' - test is duplicated twice)
        var lastDuplicate = (config.same[ind] > -1 && memo[config.same[ind]]) ? memo[config.same[ind]] : null,
          lastIndex = lastDuplicate ? lastDuplicate[1] : 0,
          index = lowcased.indexOf(equal, lastIndex),
          matches = index > -1 ? [index, index + equal.length] : null;

        if (matches) {
          lastIndex = index + equal.length;
          while ((index = lowcased.indexOf(equal, lastIndex)) > -1) {
            lastIndex = index + equal.length;
            matches.push(index);
            matches.push(index + equal.length);
          }
        }

        memo.push(matches);
        return memo;
      }, []);
    }

    /**
     * @ngdoc method
     * @name isNegative
     * @methodOf App.Core.Devv.Project
     * @description
     * Returns true if "negative" words found in examined string
     * @param {string} value
     * @param {IMatchConfig} config
     * @returns {boolean} true if "negative" words found
     */
    static isNegative(value:string, config:IMatchConfig):boolean {
      if (!value || !config)
        return null;

      var lowcased = value.toLowerCase();
      // simply check each negative words
      return _.find(config.negative, (neg:string) => lowcased.indexOf(neg) > -1) ? true : false;
    }

    /**
     * @ngdoc method
     * @name matchFootprint
     * @methodOf App.Core.Devv.Project
     * @description
     * Generates matchFootprint for Value string according to config Query
     * @param {string} value
     * @param {IMatchConfig} config
     * @returns {IMatchFootprint|null} statistics for Value string
     */
    static matchFootprint(value:string, config:IMatchConfig):IMatchFootprint {
      if (!value || !config)
        return null;

      var lowcased = value.toLowerCase();
      // If string contains at least one negative, its already unconditional
      if (_.find(config.negative, (neg:string) => lowcased.indexOf(neg) > -1))
        return null;

      var entries = SearchFilter.findEntries(value, config),
        count = 0,  // holds count of matched words
        match: number[] = [], // map of true/false values if words was found in examined string
        matchCount = 0,
        // counts distance between words
        wordsCloseness = _.reduce(entries, (memo:{count: number; last: number;}, indexes:number[]) => {

          if (indexes) {
            memo.count += Math.abs(indexes[0] - memo.last);
            memo.last = indexes[1];
            match.push(indexes.length / 2);
            matchCount += indexes.length / 2;
            count++;
          } else {
            match.push(0);
          }

          return memo;
        }, {count: 0, last: 0}).count;

      return <IMatchFootprint>{
        exactMatch: count == config.equal.length,
        count: count,
        wordsCloseness: wordsCloseness,
        entries: entries,
        match: match,
        matchCount: matchCount
      };
    }

    /**
     * @ngdoc method
     * @name getMatchConfig
     * @methodOf App.Core.Devv.Project
     * @description
     * Generates IMatchConfig for gien query string
     * @param {string} query
     * @returns {IMatchConfig}
     */
    static getMatchConfig(query:string):IMatchConfig {
      if (!query)
        return <IMatchConfig>{
          equal: [],
          negative: [],
          same: []
        };

      // find `"` to process them as pairs for "exact match" feature
      // simple search string split by space
      var exactMatches = findEntries(query, '"'),
        lastIndex = 0,
        query_length = query.length,
        items = [];

      for (var i=0,ii=exactMatches.length; i<ii; i+=2) {
        if (exactMatches[i] >=0 && exactMatches[i+1]>=0) {
          items = items.concat(query.slice(lastIndex, exactMatches[i]).split(/\s+/gi));
          items.push(query.slice(exactMatches[i] +1, exactMatches[i+1]));
          lastIndex = exactMatches[i+1] +1;
        }
      }
      items = items.concat(query.slice(lastIndex).split(/\s+/gi));

      items = _.filter(items, (str:string) => str && str.length);       // filter empty strings
      items = _.map(items, (str:string) => {return str.toLowerCase();}); // lowercase each

      // remove "negatives" form "positives"
      var negative = _.remove(items, (str:string) => str[0] == '-');
      negative = _.map(negative, (str:string) => str.slice(1)); // remover "-" char from negatives

      return <IMatchConfig>{
        equal: items,
        same: findSameEntries(items),
        negative: negative
      };
    }

    /**
     * @ngdoc method
     * @name merge
     * @methodOf App.Core.Devv.Project
     * @description
     * Merges two footprints in one.
     * NOTE: 'entries' property doens't exist in merged footprint, because indexes are for different strings
     * @param {IMatchFootprint} a
     * @param {IMatchFootprint} b
     * @returns {IMatchFootprint} merged footprint
     */
    static merge(a:IMatchFootprint, b:IMatchFootprint):IMatchFootprint {
      if (a && b) {
        if (a.match.length != b.match.length) // otherway it means footprints generated with different configs
          throw new Error('MatchFootprint should have "match" width equal length');

        var count = 0, // counts matched words
          matchCount = 0,

          match = _.map(<number[]>a.match, (val:number, index:number) => {
            var i = a.match[index] + b.match[index];
            if (i) {
              count++;
              matchCount += i;
              return i;
            }
            return 0;
          });

        return <IMatchFootprint>{
          exactMatch: a.exactMatch && b.exactMatch,
          count: count,
          wordsCloseness: a.wordsCloseness + b.wordsCloseness,
          match: match,
          matchCount: matchCount
        };
      }
      return a || b;
    }

    /**
     * @ngdoc method
     * @name compare
     * @methodOf App.Core.Devv.Project
     * @description
     * Compares two Footprints.
     * Can be used to sort by relevance.
     * @param {IMatchFootprint} a
     * @param {IMatchFootprint} b
     * @returns {number} 1 if a > b, -1 if a < b, 0 if a == b
     */
    static compare(a:IMatchFootprint, b:IMatchFootprint):number {
      if (a && b) {
        if (a.exactMatch && !b.exactMatch)
          return 1;
        if (!a.exactMatch && b.exactMatch)
          return -1;

        if (a.count > b.count)
          return 1;
        if (a.count < b.count)
          return -1;

        if (a.matchCount > b.matchCount)
          return 1;
        if (a.matchCount < b.matchCount)
          return -1;

        if (a.wordsCloseness < b.wordsCloseness)
          return 1;
        if (a.wordsCloseness > b.wordsCloseness)
          return -1;

        return 0;
      }

      if (a && !b)
        return 1;

      if (!a && b)
        return -1;

      return 0;
    }

    /**
     * @ngdoc method
     * @name highlight
     * @methodOf App.Core.Devv.Project
     * @description
     * Adds Opening/Closing string in defined indexes.
     * e.g. 'test string' + [[0, 4], [5,7]] + '<' + '>' = '<test> <st>ring'
     * @param {string} str string to be highlighted
     * @param {number[][]} indexes map of indexes
     * @param {string} firstChar
     * @param {string} lastChar
     * @returns {string} highlighted version of string
     */
    static highlight(str:string, indexes:number[][], firstChar:string, lastChar:string): string {
      if (!str || !(indexes && indexes.length) || !firstChar || !lastChar)
        return str;

      var length = str.length,
        // Flatten array of indexes, and sort them by position
        sorted = _.sortBy(_.reduce(indexes, (memo:any[], index:number[]) => {
          if (index)
            for (var i=0, ii=index.length; i<ii;i+=2) {
              if (index[i]>= 0 && index[i+1] >=0 && index[i] <= length && index[i+1] <= length) {
                memo.push({
                  first: 1,
                  index: index[i]
                });
                memo.push({
                  first: 0,
                  index: index[i+1]
                });
              }
            }

          return memo;
        }, []), 'index'),

        buffer:string[] = [],

        // slice string into buffer
        lasetIndex = _.reduce(sorted, (lastIndex:number, i:{first: number; index: number}) => {
          buffer.push(str.slice(lastIndex, i.index));
          buffer.push(i.first ? firstChar : lastChar);
          return i.index;
        }, 0);

      // add last part of string
      buffer.push(str.slice(lasetIndex));

      // convert buffer into string
      return buffer.join('');
    }

  }

}
