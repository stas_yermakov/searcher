/// <reference path="../core.d.ts" />

'use strict';

module App.Core.Devv {

  /**
   * @interface App.Core.Devv.IProject
   * @description
   * # IProject
   * Interface for Project item
   */
  export interface IProject {
    id?: string;

    Name?: string;
    Type?: string;
    "Designed by"?: string;
  }

  /**
   * @interface App.Core.Devv.IProjectSearchMatch
   * @description
   * # IProjectSearchMatch
   * Interface holds merged footPrint for properties we are searching.
   * And footPrints hashmap per each property
   */
  export interface IProjectSearchMatch {
    footPrint: IMatchFootprint;
    footPrints: {[key: string]: IMatchFootprint};
  }

  /**
   * @class App.Core.Devv.Project
   * @description
   * # Project
   * Holds static methods to work with IProject instances
   */
  export class Project {

    // List of searchable properties of IProject
    static search_properties = ['Name', 'Type', 'Designed by'];

    /**
     * @ngdoc method
     * @name searchMatch
     * @methodOf App.Core.Devv.Project
     * @description
     * Generates IProjectSearchMatch object for given project, according to config.
     * @param {IProject} project
     * @param {IMatchConfig} config could be generated with Devv.SearchFilter.getMatchConfig('query string')
     * @returns {IProjectSearchMatch}
     */
    static searchMatch(project:IProject, config:Devv.IMatchConfig):IProjectSearchMatch {
      if (!project || !config)
        return null;

      var merged:IMatchFootprint = null,
        negativeConfig = config,  // separate "negative" config, from positive, because property of IProject could
                                  // not match positive-Search, but shouldn't match negative
        positiveConfig = <any>_.extend({}, config, {negative: []}), // remove negative part of config to avoid
                                                                    // unnecessary checks while getting matchFootprint
                                                                    // for each property from IProject
        isNegative = 0,           // count "negative" properties, currently if count > 0, IProject is already unsuitable

        footPrints:{[key: string]: IMatchFootprint} =
          // iterate searchable properties, count if it matches negativeConfig, produce footPrints as hash-map
          // {[property name]: propertyFootprint}
          // process 'merged' footprint for whole
          _.reduce(Project.search_properties, (memo:any, key:string) => {
            if (project[key] && !isNegative) {
              isNegative += Devv.SearchFilter.isNegative(project[key], negativeConfig) ? 1 : 0;
              if (!isNegative) { // no need to check positive match, as we already know whole IProject is unsuitable
                memo[key] = Devv.SearchFilter.matchFootprint(project[key], positiveConfig);
                merged = Devv.SearchFilter.merge(memo[key], merged);
              }
            }

            return memo;
          }, {});

      // Assume IProject as suitable if in summary each word from query contained at least once
      if (!isNegative && merged && merged.count == config.equal.length) {
        return {
          footPrint: merged,
          footPrints: footPrints
        };
      }
      return null;
    }

    /**
     * @ngdoc method
     * @name highlight
     * @methodOf App.Core.Devv.Project
     * @description
     * Highlights each searchable property of IProject.
     * Highlight means adding HTML tags.
     * Note: hashmap for footprints should be generated before,
     *       because usually they will be taken while filtering list og Projects
     * @param {IProject} project
     * @param {any} footPrints hashmap of footprints
     * @param {string} startChar any string used as opening of highlight (e.g. '<b>')
     * @param {string} endChar any string used as closing of highlight (e.g. '</b>')
     * @returns {IProject} cloned IProject with processed properties
     */
    static highlight(project:IProject, footPrints:{[key: string]: IMatchFootprint}, startChar:string, endChar:string):IProject {
      if (!project || !footPrints)
        return project;

      // iterate through searchable properties, and generate highlighted versions
      return _.reduce(Project.search_properties, (memo:any, key:string) => {
        if (project[key] && footPrints[key]) { // there is no such prop or it wasn't matched
          memo[key] = Devv.SearchFilter.highlight(project[key], footPrints[key].entries, startChar, endChar);
        }

        return memo;
      }, _.clone(project)); // have to additionally clone, to not omit other not-highlighted properties like 'id'
    }

    /**
     * @ngdoc method
     * @name searchProjects
     * @methodOf App.Core.Devv.Project
     * @description
     * Filters array of Projects according to Querry string.
     * Optionally Highlights properties of matched.
     * @param {IProject[]} projects array of projects to be filtered
     * @param {boolean} query
     * @param {string} highlight
     * @param {string} startChar any string used as opening of highlight (e.g. '<b>')
     * @param {string} endChar any string used as closing of highlight (e.g. '</b>')
     * @returns {IProject[]} array of filtered propjects
     */
    static searchProjects(projects:IProject[], query:string, highlight?:boolean, startChar?:string, endChar?:string):IProject[] {
      if (projects && projects.length && query) {
        var config = Devv.SearchFilter.getMatchConfig(query), // generate config, so need to make it for each item in array

          // iterate projects array, filter them and save footPrint to sort
          search = _.reduce(projects, (memo:any[], project:IProject) => {
            var match = Devv.Project.searchMatch(project, config);
            if (match) {
              memo.push({
                project: highlight ? Devv.Project.highlight(project, match.footPrints, startChar, endChar) : project,
                footPrint: match.footPrint
              });
            }
            return memo;
          }, []);

        // sort by merged footPrint
        search = search.sort((a:any, b:any) => {
          return Devv.SearchFilter.compare(b.footPrint, a.footPrint);
        });

        // make list of filtered Projects, as search holds {project, footPrint}
        return _.map(search, 'project');
      }

      return projects;
    }
  }

}
