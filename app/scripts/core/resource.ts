/// <reference path="core.d.ts" />
/// <reference path="../app.ts" />

'use strict';

module App.Core {
  export interface IResource<T> {
    data: T;

    query: any;
    loading: boolean ;
    error: any;
    attempts: number;

    set (data:T);
    get (): T;

    setLoading ();
    onFail (reason:any);
    onDone (data?:T);
    clear ();
    abort ();

    loadData(promise:ng.IPromise<T>): (ng.IPromise<T>);
    loadData(promise:ng.IPromise<T>, ...args:any[]): (ng.IPromise<T>);
  }

  /**
   * @class App.Core.Resource
   * @description
   * # Resource
   * Holds properties useful to track while AJAX calls.
   */

  export class Resource<T> implements IResource<T> {
    query:any;
    loading:boolean = false;
    error:any = null;
    attempts:number = 0;
    private promise:ng.IPromise<T>;
    private promise_id = 1;

    constructor(public data?:T) {
    }

    /**
     * @ngdoc method
     * @name setLoading
     * @methodOf App.Core.Resource
     * @description
     * Sets 'loading' flag to true, and flushes error
     */
    setLoading() {
      this.attempts++;
      this.loading = true;
      this.error = null;
    }

    /**
     * @ngdoc method
     * @name onFail
     * @methodOf App.Core.Resource
     * @description
     * Called after Load was failed
     * @param {any} reason
     */
    onFail(reason:any) {
      this.set(null);
      this.error = reason || true;
      this.loading = false;
      this.promise = null;
    }

    /**
     * @ngdoc method
     * @name onDone
     * @methodOf App.Core.Resource
     * @description
     * Called after Load was success
     * @param {T} data
     */
    onDone(data?:T) {
      this.set(data);
      this.loading = false;
      this.error = null;
      this.promise = null;
    }

    /**
     * @ngdoc method
     * @name clear
     * @methodOf App.Core.Resource
     * @description
     * Flushes properties
     */
    clear() {
      this.abort();
      this.attempts = 0;
      this.data = null;
    }

    /**
     * @ngdoc method
     * @name set
     * @methodOf App.Core.Resource
     * @description
     * Set data property
     * @param {T} data
     */
    set(data:T) {
      this.data = data;
    }

    /**
     * @ngdoc method
     * @name get
     * @methodOf App.Core.Resource
     * @description
     * Get data property
     * @returns {T}
     */
    get():T {
      return this.data;
    }

    /**
     * @ngdoc method
     * @name abort
     * @methodOf App.Core.Resource
     * @description
     * Aborts waiting for current Loading
     */
    abort() {
      this.query = null;
      this.loading = false;
      this.error = null;
      this.promise = null;
      this.promise_id++;
    }

    /**
     * @ngdoc method
     * @name loadData
     * @methodOf App.Core.Resource
     * @description
     * # loadData
     * Start watching Promise that should return data for that resource
     * @param {ng.IPromise<T>|JQueryPromise<T>} promise promise to be tracked
     * @param {...any[]} args optional parameters
     * @returns {ng.IPromise<T>|JQueryPromise<T>}
     */
    public loadData(promise:ng.IPromise<T>, ...args:any[]):ng.IPromise<T> {
      this.query = args;
      this.setLoading();
      var prom_id = ++this.promise_id;

      return this.promise = (<any>promise)
        .then((data?:T) => {
          if (prom_id == this.promise_id)
            this.onDone(data);
          return data;
        }, (error?:any) => {
          if (prom_id == this.promise_id)
            this.onFail(error);
          return $.Deferred().reject(error);
        });
    }

    /**
     * @ngdoc method
     * @name isSuccess
     * @methodOf App.Core.Resource
     * @description
     * @returns {boolean} true if previous load was success
     */
    isSuccess() {
      return !this.loading && this.attempts && !this.error ? true : false;
    }

    /**
     * @ngdoc method
     * @name isFailed
     * @methodOf App.Core.Resource
     * @description
     * @returns {boolean} true if previous load was failed
     */
    isFailed() {
      return !this.loading && this.attempts && this.error ? true : false;
    }
  }

}
