/// <reference path="../app.ts" />

'use strict';

module Searcher {

  /**
   * @ngdoc service
   * @name searcherApp.service:projectsSearch
   * @class Searcher.ProjectsSearch
   * @description
   * # projectsSearch
   * Service that provides connection to Projects API
   */
  export class ProjectsSearch {
    static $inject = [
      '$log',
      '$http',
      '$timeout'
    ];

    constructor(
      private $log: ng.ILogService,
      private $http: ng.IHttpService,
      private $timeout: ng.ITimeoutService
    ) {
    }

    /**
     * @ngdoc method
     * @name getProjects
     * @methodOf Searcher.ProjectsSearch
     * @description
     * # getProjects
     * Gets list of Projects, filtered by query
     *
     * @param {string} query to search for
     * @returns {IPromise<TResult>} Promise to return list of IProject
     */
    getProjects(query: string): ng.IPromise<App.Core.Devv.IProject[]> {
      return this.$timeout(angular.noop, 700) // TODO: remove from production, Fakes network delay, when testing locally
        .then(() => {
          return this.$http.get('api/search-data.json', {
            data: {
              query: query
            }
          })
            .then((resp: ng.IHttpPromiseCallbackArg<App.Core.Devv.IProject[]>) => {
              // Filter array of IProject -s according to search query, and highlight properties with HTML <mark> tag
              return App.Core.Devv.Project.searchProjects(resp.data, query, true, '<mark>', '</mark>');
            });
        });
    }
  }
}

angular.module('searcherApp')
  .service('projectsSearch', Searcher.ProjectsSearch);
