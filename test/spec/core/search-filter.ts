/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../../app/scripts/inc.d.ts" />

'use strict';

describe('Class: SearchFilter', () => {

  var SearchFilter = App.Core.Devv.SearchFilter;

  describe('getMatchConfig', () => {

    it('doesn\'t afraid null', () => {
      expect(SearchFilter.getMatchConfig(null))
        .toEqual({
          "equal": [],
          "negative": [],
          "same": []
        });
    });

    it('returns match config', () => {
      expect(SearchFilter.getMatchConfig('Test Line'))
        .toEqual({
          "equal": [
            "test",
            "line"
          ],
          "negative": [],
          "same": [-1, -1]
        });
    });

    it('returns match config', () => {
      expect(SearchFilter.getMatchConfig('   Test    Line   and one more'))
        .toEqual({
          "equal": [
            "test",
            "line",
            "and",
            "one",
            "more"
          ],
          "negative": [],
          "same": [-1, -1, -1, -1, -1]
        });
    });

    it('returns match config with negative', () => {
      expect(SearchFilter.getMatchConfig('Test -not Line -Not'))
        .toEqual({
          "equal": [
            "test",
            "line"
          ],
          "negative": [
            "not",
            "not"
          ],
          "same": [-1, -1]
        });
    });

    it('returns "same" property', () => {
      expect(SearchFilter.getMatchConfig('Test Line Line'))
        .toEqual({
          "equal": [
            "test",
            "line",
            "line"
          ],
          "negative": [],
          "same": [-1, -1, 1]
        });
    });

    it('supports exact matches', () => {
      expect(SearchFilter.getMatchConfig('Test "Line Line"'))
        .toEqual({
          "equal": [
            "test",
            "line line"
          ],
          "negative": [],
          "same": [-1, -1]
        });

      expect(SearchFilter.getMatchConfig('"Test Line"'))
        .toEqual({
          "equal": [
            "test line"
          ],
          "negative": [],
          "same": [-1]
        });

      expect(SearchFilter.getMatchConfig('"Test Line'))
        .toEqual({
          "equal": [
            "\"test",
            "line"
          ],
          "negative": [],
          "same": [-1, -1]
        });

      expect(SearchFilter.getMatchConfig('Test "Line'))
        .toEqual({
          "equal": [
            "test",
            "\"line"
          ],
          "negative": [],
          "same": [-1, -1]
        });
    });

  });

  describe('findEntries', () => {

    it('doesn\'t afraid null', () => {
      expect(SearchFilter.findEntries(null, null))
        .toBeNull();
    });

    it('finds entries', () => {
      expect(SearchFilter.findEntries('Test Line', SearchFilter.getMatchConfig('Test Line')))
        .toEqual([ [ 0, 4 ], [ 5, 9 ] ] );
    });

    it('finds entries, not found return as null item of array', () => {
      expect(SearchFilter.findEntries('Line Test', SearchFilter.getMatchConfig('Test more Line')))
        .toEqual([ [ 5, 9 ], null, [ 0, 4 ] ]);
    });

    it('finds entries', () => {
      expect(SearchFilter.findEntries("Other test query", SearchFilter.getMatchConfig("test query here")))
        .toEqual([ [6,10], [11,16], null]);
    });

    it('finds entries', () => {
      expect(SearchFilter.findEntries("Other test query", SearchFilter.getMatchConfig('"test query"')))
        .toEqual([ [6,16] ]);
    });
  });


  describe('matchFootprint', () => {

    it('doesn\'t afraid null', () => {
      expect(SearchFilter.matchFootprint(null, null))
        .toBeNull();
    });

    it('filter negatives', () => {
      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test -Line')))
        .toBeNull();
    });

    it('filter negatives', () => {
      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test Line -not')))
        .toBeDefined();
    });

    it('tests for exact Match (each word form query should be included in string)', () => {
      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test LiNe'))
        .exactMatch)
        .toBeTruthy();
    });

    it('tests for exact Match, ignoring order', () => {
      expect(SearchFilter.matchFootprint(' Test Line , ', SearchFilter.getMatchConfig('LiNe Test'))
        .exactMatch)
        .toBeTruthy();
    });

    it('tests for exact Match, ignoring order', () => {
      expect(SearchFilter.matchFootprint(' Test Line , ', SearchFilter.getMatchConfig('LiNe Test one more'))
        .exactMatch)
        .toBeFalsy();
    });

    it('counts entries', () => {
      expect(SearchFilter.matchFootprint(' Test Line', SearchFilter.getMatchConfig('LiNe Test'))
        .count)
        .toEqual(2);

      expect(SearchFilter.matchFootprint(' Test Line', SearchFilter.getMatchConfig('LiNe Test one more'))
        .count)
        .toEqual(2);

      expect(SearchFilter.matchFootprint(' Test   Line', SearchFilter.getMatchConfig('  LiNe   Test one   more'))
        .count)
        .toEqual(2);
    });

    it('counts wordsCloseness', () => {

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test Line'))
        .wordsCloseness)
        .toEqual(1);

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Line Test'))
        .wordsCloseness)
        .toEqual(14);

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test Test more'))
        .wordsCloseness)
        .toEqual(0);

      expect(SearchFilter.matchFootprint('Test Line test', SearchFilter.getMatchConfig('Test Test more'))
        .wordsCloseness)
        .toEqual(6);

      expect(SearchFilter.matchFootprint(' Test other Line', SearchFilter.getMatchConfig('Test Test more'))
        .wordsCloseness)
        .toEqual(1);

      expect(SearchFilter.matchFootprint(' Test 12 122 other Line', SearchFilter.getMatchConfig('Test 122 more 12'))
        .wordsCloseness)
        .toEqual(11);

    });

    it('returns match array', () => {

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test Line'))
        .match)
        .toEqual([ 1, 1 ]);

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Line Test'))
        .match)
        .toEqual([ 1, 1 ]);

      expect(SearchFilter.matchFootprint('Test Line', SearchFilter.getMatchConfig('Test Test more'))
        .match)
        .toEqual([ 1, 0, 0 ]);

      expect(SearchFilter.matchFootprint('Test Line test', SearchFilter.getMatchConfig('Test Test more'))
        .match)
        .toEqual([ 2, 1, 0 ]);

      expect(SearchFilter.matchFootprint(' more Test other Line', SearchFilter.getMatchConfig('Test Test more'))
        .match)
        .toEqual([ 1, 0, 1 ]);

      expect(SearchFilter.matchFootprint(' Test 12 122 other Line', SearchFilter.getMatchConfig('Test 122 more 12'))
        .match)
        .toEqual([ 1, 1, 0, 2 ]);

    });

  });

  describe('merge', () => {

    it('doesn\'t afraid null', () => {

      expect(SearchFilter.merge(null, null))
        .toBeNull();

    });

    it('merges footprints 1', () => {

      var config = SearchFilter.getMatchConfig('Test Line'),
        a = SearchFilter.matchFootprint('Test Line', config),
        b = SearchFilter.matchFootprint('other Test Line', config),
        merged = SearchFilter.merge(a, b);

      expect(merged.exactMatch)
        .toEqual(true);

      expect(merged.count)
        .toEqual(2);

      expect(merged.entries)
        .toBeUndefined();

      expect(merged.match)
        .toEqual([ 2, 2 ]);

      expect(merged.wordsCloseness)
        .toEqual(a.wordsCloseness + b.wordsCloseness);

    });

    it('merges footprints 2', () => {

      var config = SearchFilter.getMatchConfig('  Test Line test'),
        a = SearchFilter.matchFootprint('Test Line', config),
        b = SearchFilter.matchFootprint('other Test Line test', config),
        merged = SearchFilter.merge(a, b);

      expect(merged.exactMatch)
        .toEqual(false);

      expect(merged.count)
        .toEqual(3);

      expect(merged.entries)
        .toBeUndefined();

      expect(merged.match)
        .toEqual([ 3, 2, 1 ]);

      expect(merged.wordsCloseness)
        .toEqual(a.wordsCloseness + b.wordsCloseness);

    });

    it('merges footprints 3', () => {

      var config = SearchFilter.getMatchConfig(' Line more Test test'),
        a = SearchFilter.matchFootprint('Test Line', config),
        b = SearchFilter.matchFootprint('other Test Line', config),
        merged = SearchFilter.merge(a, b);

      expect(merged.exactMatch)
        .toEqual(false);

      expect(merged.count)
        .toEqual(2);

      expect(merged.entries)
        .toBeUndefined();

      expect(merged.match)
        .toEqual([ 2, 0, 2, 0 ]);

      expect(merged.wordsCloseness)
        .toEqual(a.wordsCloseness + b.wordsCloseness);

    });

  });

  describe('compare', () => {

    it('doesn\'t afraid null', () => {

      expect(SearchFilter.compare(null, null))
        .toEqual(0);

      expect(SearchFilter.compare({
        exactMatch: false,
        count: 1,
        matchCount: 1,
        wordsCloseness: 1
      }, null))
        .toEqual(1);

      expect(SearchFilter.compare(null, {
        exactMatch: false,
        count: 1,
        matchCount: 1,
        wordsCloseness: 1
      }))
        .toEqual(-1);

    });

    it('compares footPrints', () => {

      expect(SearchFilter.compare({
        exactMatch: false,
        count: 1,
        matchCount: 2,
        wordsCloseness: 1
      }, {
        exactMatch: false,
        count: 1,
        matchCount: 2,
        wordsCloseness: 1
      }))
        .toEqual(0);

      expect(SearchFilter.compare({
        exactMatch: true,
        count: 1,
        matchCount: 2,
        wordsCloseness: 1
      }, {
        exactMatch: false,
        count: 1,
        matchCount: 2,
        wordsCloseness: 1
      }))
        .toEqual(1);

      expect(SearchFilter.compare({
        exactMatch: true,
        count: 1,
        matchCount: 1,
        wordsCloseness: 1
      }, {
        exactMatch: true,
        count: 2,
        matchCount: 1,
        wordsCloseness: 1
      }))
        .toEqual(-1);

      expect(SearchFilter.compare({
        exactMatch: true,
        count: 1,
        matchCount: 2,
        wordsCloseness: 5
      }, {
        exactMatch: true,
        count: 1,
        matchCount: 1,
        wordsCloseness: 1
      }))
        .toEqual(1);

      expect(SearchFilter.compare({
        exactMatch: false,
        count: 2,
        matchCount: 1,
        wordsCloseness: 5
      }, {
        exactMatch: false,
        count: 2,
        matchCount: 1,
        wordsCloseness: 1
      }))
        .toEqual(-1);

      expect(SearchFilter.compare({
        exactMatch: false,
        count: 2,
        matchCount: 1,
        wordsCloseness: 0
      }, {
        exactMatch: true,
        count: 2,
        matchCount: 1,
        wordsCloseness: 15
      }))
        .toEqual(-1);

    });

    it('compares footPrints 2', () => {

      var config = SearchFilter.getMatchConfig(' Line test');

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Test Line', config),
        SearchFilter.matchFootprint('Test line more', config)))
        .toEqual(0);

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Test Line', config),
        SearchFilter.matchFootprint('Test  line', config)))
        .toEqual(1);

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Test Lin', config),
        SearchFilter.matchFootprint('Test  line', config)))
        .toEqual(-1);

    });

    it('compares footPrints 3', () => {

      var config = SearchFilter.getMatchConfig(' Line test more test');

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Test Line', config),
        SearchFilter.matchFootprint('Test more', config)))
        .toEqual(-1);

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Line Test test', config),
        SearchFilter.matchFootprint('Test  line more', config)))
        .toEqual(1);

      expect(SearchFilter.compare(
        SearchFilter.matchFootprint('Test Lin', config),
        SearchFilter.matchFootprint('Test  line', config)))
        .toEqual(-1);

    });

  });

  describe('highlight', () => {

    it('doesn\'t afraid null', () => {

      expect(SearchFilter.highlight(null, [], '<', '>'))
        .toBeNull();

      expect(SearchFilter.highlight('test', [], '<', '>'))
        .toEqual('test');

      expect(SearchFilter.highlight('test', [[0, 1]], null, '>'))
        .toEqual('test');

    });

    it('highlights string', () => {

      expect(SearchFilter.highlight('Test line here', [[0, 4], [5, 9]], '<', '>'))
        .toEqual('<Test> <line> here');

      expect(SearchFilter.highlight('Test line here', [[0, 4], [4, 9]], '<', '>'))
        .toEqual('<Test>< line> here');

      expect(SearchFilter.highlight('Test line here', [[0, 4], [3, 9]], '<', '>'))
        .toEqual('<Tes<t> line> here');

      expect(SearchFilter.highlight('Test line here', [[0, 5], [5, 9]], '<', '>'))
        .toEqual('<Test ><line> here');

      expect(SearchFilter.highlight('Test line here', [[5, 9], [0, 5]], '<', '>'))
        .toEqual('<Test <>line> here');

      expect(SearchFilter.highlight('Test line here', [[5, 9], [0, 6]], '<', '>'))
        .toEqual('<Test <l>ine> here');

      expect(SearchFilter.highlight('Test line here', [[5, 9], [6, 7]], '<', '>'))
        .toEqual('Test <l<i>ne> here');

      expect(SearchFilter.highlight('Test line here', [[0, 4], [5, 9], [6, 8]], '<', '>'))
        .toEqual('<Test> <l<in>e> here');

      expect(SearchFilter.highlight('Test line here', [[0, 4], [5, 9], [6, 9]], '<', '>'))
        .toEqual('<Test> <l<ine>> here');

      expect(SearchFilter.highlight('Test', [[0, 4], [5, 9], [6, 9]], '<', '>'))
        .toEqual('<Test>');

    });

  });

});
