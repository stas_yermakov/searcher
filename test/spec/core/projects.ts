/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/jasmine/jasmine.d.ts" />
/// <reference path="../../../app/scripts/inc.d.ts" />

'use strict';

describe('Class: Project', () => {

  var SearchFilter = App.Core.Devv.SearchFilter,
    Project = App.Core.Devv.Project;

  describe('searchMatch', () => {

    it('doesn\'t afraid null', () => {
      expect(Project.searchMatch(null, {
        "equal": [],
        "negative": [],
        "same": []
      }))
        .toBeNull();

      expect(Project.searchMatch({Name: 'Some name'}, null))
        .toBeNull();

    });

    it('tests to match', () => {
      var config = SearchFilter.getMatchConfig('test line');

      expect(Project.searchMatch({
        Name: 'test'
      }, config))
        .toBeNull();

      expect(Project.searchMatch({
        Name: 'test line'
      }, config))
        .toBeDefined();

      expect(Project.searchMatch({
        Name: 'test',
        Type: ' test'
      }, config))
        .toBeNull();

      expect(Project.searchMatch({
        Name: 'test',
        Type: ' line'
      }, config))
        .toBeDefined();

      expect(Project.searchMatch({
        Name: 'test',
        Type: ' line',
        "Designed by": 'line'
      }, config))
        .toBeDefined();

      expect(Project.searchMatch({
        Name: 'test',
        Type: ' more',
        "Designed by": ' test line'
      }, config))
        .toBeDefined();

    });

    it('returns merged footprint', () => {
      var config = SearchFilter.getMatchConfig('test line'),
        footPrint = Project.searchMatch({
          Name: 'test line'
        }, config).footPrint;

      expect(footPrint.exactMatch)
        .toEqual(true);
      expect(footPrint.count)
        .toEqual(2);
      expect(footPrint.match)
        .toEqual([1, 1]);

    });

    it('returns merged footprint for multiple lines', () => {
      var config = SearchFilter.getMatchConfig('test line'),
        footPrint = Project.searchMatch({
          Name: 'test',
          Type: ' line'
        }, config).footPrint;

      expect(footPrint.exactMatch)
        .toEqual(false);
      expect(footPrint.count)
        .toEqual(2);
      expect(footPrint.match)
        .toEqual([1, 1]);

    });

    it('returns merged footprint for multiple lines', () => {
      var config = SearchFilter.getMatchConfig('line test more test'),
        footPrint = Project.searchMatch({
          Name: 'test',
          Type: 'more line',
          "Designed by": 'test test'
        }, config).footPrint;

      expect(footPrint.exactMatch)
        .toEqual(false);
      expect(footPrint.count)
        .toEqual(4);
      expect(footPrint.match)
        .toEqual([1, 3, 1, 1]);

    });

    it('returns footprints hashmap', () => {
      var config = SearchFilter.getMatchConfig('line test more test'),
        footPrints = Project.searchMatch({
          Name: 'test',
          Type: 'more line',
          "Designed by": 'test test'
        }, config).footPrints;

      expect(footPrints)
        .toBeDefined();
      expect(footPrints['Name'])
        .toBeDefined();
      expect(footPrints['Type'])
        .toBeDefined();
      expect(footPrints['Designed by'])
        .toBeDefined();

      expect(footPrints['Name'].count)
        .toEqual(1);
      expect(footPrints['Type'].count)
        .toEqual(2);
      expect(footPrints['Designed by'].count)
        .toEqual(2);

    });

  });

  describe('highlight', () => {

    it('doesn\'t afraid null', () => {

      expect(Project.highlight(null, null, '<', '>'))
        .toBeNull();

      expect(Project.highlight({
        Name: 'test'
      }, null, '<', '>'))
        .toBeDefined();

    });

    it('highlights each field', () => {
      var config = SearchFilter.getMatchConfig('line test more test'),
        project = {
          Name: 'test',
          Type: 'more line',
          "Designed by": 'test test other'
        },
        footPrints = Project.searchMatch(project, config).footPrints,
        highlighted = Project.highlight(project, footPrints, '<', '>');

      expect(highlighted['Name'])
        .toEqual('<test>');
      expect(highlighted['Type'])
        .toEqual('<more> <line>');
      expect(highlighted['Designed by'])
        .toEqual('<test> <<test>> other');

    });

  });

  describe('searchProjects', () => {

    it('doesn\'t afraid null', () => {

      expect(Project.searchProjects(null, null))
        .toBeNull();

      expect(Project.searchProjects([{
        Name: 'test'
      }], null))
        .toBeDefined();

    });

    it('finds and sorts projects', () => {

      expect(_.map(Project.searchProjects([{
        id: '1',
        Name: 'test',
        Type: 'more line',
        "Designed by": 'test test other'
      }, {
        id: '2',
        Name: 'more ',
        Type: 'test line',
        "Designed by": 'test test other'
      }], 'test'), 'id'))
        .toEqual(['1', '2']);

      expect(_.map(Project.searchProjects([{
        id: '1',
        Name: 'test',
        Type: 'more line',
        "Designed by": 'test test other'
      }, {
        id: '2',
        Name: '',
        Type: 'test line',
        "Designed by": 'test test other'
      }], 'test more'), 'id'))
        .toEqual(['1']);

      expect(_.map(Project.searchProjects([
        {
          id: '1',
          Name: 'test',
          Type: 'line more',
          "Designed by": 'test test other'
        }, {
          id: '2',
          Name: '',
          Type: 'test line',
          "Designed by": 'test test other'
        },
        {
          id: '3',
          Name: 'more',
          Type: 'test line',
          "Designed by": 'test test other'
        }], 'test more other'), 'id'))
        .toEqual(['3', '1']);

      expect(_.map(Project.searchProjects([
        {
          id: '1',
          Name: 'test',
          Type: 'line more',
          "Designed by": 'test other test'
        }, {
          id: '2',
          Name: '',
          Type: 'test line',
          "Designed by": 'test test other'
        },
        {
          id: '3',
          Name: 'more',
          Type: 'test line',
          "Designed by": 'test test   other'
        }], 'test more other test'), 'id'))
        .toEqual(['1', '3']);

    });

    it('finds, sorts and highlights projects', () => {

      expect(Project.searchProjects([
        {
          id: '1',
          Name: 'test',
          Type: 'line more',
          "Designed by": 'test other test'
        }, {
          id: '2',
          Name: '',
          Type: 'test line',
          "Designed by": 'test test other'
        },
        {
          id: '3',
          Name: 'more',
          Type: 'test line',
          "Designed by": 'test test   other'
        }], 'test more other test', true, '<', '>'))
        .toEqual([{
          id: '1',
          Name: '<test>',
          Type: 'line <more>',
          "Designed by": '<test> <other> <<test>>'
        },
          {
            id: '3',
            Name: '<more>',
            Type: '<test> line',
            "Designed by": '<test> <<test>>   <other>'
          }]);

    });

    it('finds with exact match, sorts and highlights projects', () => {

      expect(Project.searchProjects([
        {
          id: '1',
          Name: 'test',
          Type: 'line more other',
          "Designed by": 'test other test'
        }, {
          id: '2',
          Name: '',
          Type: 'test line',
          "Designed by": 'test test other'
        },
        {
          id: '3',
          Name: 'more other',
          Type: 'test line',
          "Designed by": 'test test   other'
        }], 'test "more other" test', true, '<', '>'))
        .toEqual([{
          id: '3',
          Name: '<more other>',
          Type: '<test> line',
          "Designed by": '<test> <<test>>   other'
        },
          {
          id: '1',
          Name: '<test>',
          Type: 'line <more other>',
          "Designed by": '<test> other <<test>>'
        }]);

    });

    it('filters out negatives', () => {

      expect(_.map(Project.searchProjects([
        {
          id: '1',
          Name: 'test line',
          Type: 'more',
          "Designed by": 'test other test'
        }, {
          id: '2',
          Name: 'other',
          Type: 'more test',
          "Designed by": 'test test other'
        },
        {
          id: '3',
          Name: 'more',
          Type: 'test line',
          "Designed by": 'test test other'
        },
        {
          id: '4',
          Name: 'more',
          Type: 'test',
          "Designed by": 'test test other'
        }], 'test -line'), 'id'))
        .toEqual(['4', '2']);


    });

  });

});
