# Searcher

Test Project for Devv Front End Developer.

## Objective

Make json data searchable (break it into HTML and CSS)

## Minimum requirements:

* Implementation must use HTML, CSS and JavaScript (all three)
* Design implemented as per attached PSD
* Use AJAX, no search button.
* Search logic should be implemented in JavaScript
* A search for Lisp Common should match a programming language named "Common Lisp"
* Your solution will be tested on a server running Ubuntu, Apache 2.4.7 and PHP 5.5
* Deliver your solution as a zip or tar.gz file
* We will unpack your solution and run it from http://ourserver/yourname/

## Meriting:

* Writing code with reusability in mind
* Search match precision
* Search results ordered by relevance
* Support for exact matches, eg. Interpreted "Thomas Eugene", which should match "BASIC", but not "Haskell"
* Match in different fields, eg. Scripting Microsoft should return all scripting languages designed by "Microsoft"
* Support for negative searches, eg. john -array, which should match "BASIC", "Haskell", "Lisp" and "S-Lang", but not 
"Chapel", "Fortran" or "S".
* Solution elegance
* Visual design

## Technical Details

 **Angular** used as Front End Framework. To show search UI, used '/search' route, that shows 'app/views/search.html' 
 and behaves according to 'app/scripts/controllers/search.ts' controller.
 To show list of found items, used 'project-compact' directive.
 
 **Twitter Bootstrap** used as CSS framework
 
 **Lodash** used as library for processing arrays and collections.
 
## Scenario
 
 * User enters Query string to search projects
 * `\app\scripts\controllers\search.ts` controller with scope $watcher reacts on change of Query string. 
    Call loading list of projects debounced to allow user finish typing query.
 * `\app\scripts\controllers\search.ts` controller calls `getProjects` method of 
   `\app\scripts\services\projects-search.ts` service, that provides connection to API
 * `\app\scripts\services\projects-search.ts` service loads data via AJAX GET method, because all files are local,
   additional timeout added to fake network delay
 * AJAX GET `/api/search-data.json` will load Projects list, physically file located at `\mocks\search-data.json`
   but hosted by `grunt connect` to `api` folder
 * because we using static `search-data.json`, we have to filter it in service on client
 * filtering will be done by search query, additionally each property of Projects will be higlighted with HTML `<mark>` 
   tag `App.Core.Devv.Project.searchProjects(data, query, true, '<mark>', '</mark>')`
 * additionally `App.Core.Devv.Project.searchProjects` will sort projects by relevance 
   * first will be project, property of which contains whole Query 
      (e.g. `{Name: 'Some Project Name'} searched by 'some name'` is more relevant then 
      `{Name: 'Some Project', Type: `Name`} searched by 'some name'`, while both are conditional)
   * earlier will be project, matches in which are closer to string beginning and closer to each other
      (e.g. `{Name: 'Some Name Project'} searched by 'some name'` is more relevant then 
      `{Name: 'Project Some Name'} searched by 'some name'`)
 * filtered list of Projects will be returned to `\app\scripts\controllers\search.ts` controller,
   it will render them using `ng-repeat` and `\app\scripts\directives\projects.ts` directive
 
## Development Tools Used
 
 * **Typescript** as preprocessor for JS code
 * **Less** as preprocessor for CSS styles
 * **Grunt** as task runner
 * **Karma** as tests runner
 * **Jasmine** as BDD framework for testing
 * **TSD** as Typescript definitions manager
 * **bower** as package manager

## Build & development

 * Run `grunt` to test and Build
 * Run `grunt build` to Build
 * Run `grunt serve` for preview

## Testing

* Run `grunt test` to unit test with karma
